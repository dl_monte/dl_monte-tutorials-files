awk -v binwidth=$2 '{value[int($2/binwidth)+1]++;if($2/binwidth>maxbin)maxbin=int($2/binwidth)+1;} END{for(x=1;x<=maxbin;x++)print (x-0.5)*binwidth,value[x]/NR;}' $1 > hist_$1
