
                   ===================================================================
                   * * *                                                         * * *
                   * * *            D L - M O N T E - v e r. 2.0 (5)             * * *
                   * * *                                                         * * *
                   * * *           STFC / CCP5 - Daresbury Laboratory            * * *
                   * * *                 program library  package                * * *
                   * * *                                                         * * *
                   * * *     general purpose Monte Carlo simulation software     * * *
                   * * *                                                         * * *
                   * * *     version:  2.05 (beta) / February 2018               * * *
                   * * *     author :  J. A. Purton (2007-2015)                  * * *
                   * * *     -----------------------------------------------     * * *
                   * * *     contributors:                                       * * *
                   * * *     -------------                                       * * *
                   * * *     A. V. Brukhno   - Free Energy Diff, planar slabs    * * *
                   * * *     T. L. Underwood - Lattice/Phase-Switch MC methods   * * *
                   * * *     R. J. Grant     - extra potentials, optimizations   * * *
                   * * *     -----------------------------------------------     * * *
                   * * *                                                         * * *
                   * * *     current execution:                                  * * *
                   * * *     ==================                                  * * *
                   * * *     number of nodes/CPUs/cores:          1              * * *
                   * * *     number of threads per node:          1              * * *
                   * * *     number of threads employed:          1              * * *
                   * * *                                                         * * *
                   * * * ======================================================= * * *
                   * * * Publishing data obtained with DL_MONTE? - please cite:  * * *
                   * * * ------------------------------------------------------- * * *
                   * * *        Purton, J. A., Crabtree J. C. and Parker, S. C., * * *
                   * * *        Molecular Simulation 2013, 39 (14-15), 1240-1252 * * *
                   * * *                `DL_MONTE: A general purpose program for * * *
                   * * *                         parallel Monte Carlo simulation`* * *
                   * * * ------------------------------------------------------- * * *
                   * * * if using Lattice-Switch method, please also cite:       * * *
                   * * * ------------------------------------------------------- * * *
                   * * *              Bruce A.D., Wilding N.B. and G.J. Ackland, * * *
                   * * *                         Phys. Rev. Lett. 1997, 79, 3002 * * *
                   * * *    `Free Energy of Crystalline Solids: A Lattice-Switch * * *
                   * * *                                      Monte Carlo Method`* * *
                   * * * ------------------------------------------------------- * * *
                   * * * if using Planar Pore (slit) geometry, please also cite: * * *
                   * * * ------------------------------------------------------- * * *
                   * * *                                         Broukhno A. V., * * *
                   * * *     `Free Energy and Surface Forces in polymer systems: * * *
                   * * *                          Monte Carlo Simulation Studies`* * *
                   * * *                           PhD Thesis, Lund, Sweden 2003 * * *
                   * * *                                                         * * *
                   * * *    Brukhno A. V., Akinshina A., Coldrick Z., Nelson A., * * *
                   * * *                                             and Auer S. * * *
                   * * *                              Soft Matter. 2011, 7, 1006 * * *
                   * * *               `Phase phenomena in supported lipid films * * *
                   * * *                        under varying electric potential`* * *
                   ===================================================================

 ----------------------------------------------------------------------------------------------------
 FIELD title  (100 symbols max) :
 ----------------------------------------------------------------------------------------------------
'spc/e water model'
 ----------------------------------------------------------------------------------------------------

 reading species:   1 - molecule 'SPCE' of          3 atoms (         3 max)

 ------------------


 ====================================================================================================
 atom data :
 ====================================================================================================
 element           mass             charge
 --------------------------------------------------
  OW              16.0000        -0.8476
  HW               1.0000         0.4238


 ====================================================================================================
 molecule data :
 ====================================================================================================
 molecule name / rigid / exclude VDW & Coulomb
 --------------------------------------------------
 'SPCE    '    /   T   /  T
 --------------------------------------------------



 ----------------------------------------------------------------------------------------------------
 nonbonded (vdw) potentials :
 ----------------------------------------------------------------------------------------------------
 OW      OW         lj                0.500000       0.650000       3.166000       0.000000       0.000000       0.000000

 VdW repulsion capped at D_core(  1) =     1.919760 by E_cap = 0.100000E+06

 VdW truncating & shifting term(  1) = 0.261578E+00 at R_cut = 0.100000E+02

 OW      HW         lj                0.500000       0.000000       0.100000       0.000000       0.000000       0.000000

 VdW repulsion capped at D_core(  2) =     0.000000 by E_cap = 0.100000E+06

 VdW truncating & shifting term(  2) = 0.000000E+00 at R_cut = 0.100000E+02

 HW      HW         lj                0.500000       0.000000       0.100000       0.000000       0.000000       0.000000

 VdW repulsion capped at D_core(  3) =     0.000000 by E_cap = 0.100000E+06

 VdW truncating & shifting term(  3) = 0.000000E+00 at R_cut = 0.100000E+02



 external potentials 


 please reference the following paper 


 S.J. Cox, S.M. Kathmann, J.A. Purton, M.J. Gillan and A. Michaelides 


 Phys. Chem. Cem. Phys. 2012, 14, 7944-7949 

         OW       lj              1.000000       3.200000       0.000000       0.000000       0.000000
                                  0.000000       0.000000       0.000000       0.000000       0.000000


 VdW(Ext) repulsion capped at D_core(  1) =      2.00925 by E_cap = 100000.00000

         HW       lj              0.001000       0.001000       0.000000       0.000000       0.000000
                                  0.000000       0.000000       0.000000       0.000000       0.000000


 VdW(Ext) repulsion capped at D_core(  2) =      0.00035 by E_cap = 100000.00000



 --------------------------------------------------
 configuration box No.          1
 --------------------------------------------------

 the maximum no of atoms/molecules in this cell:     3000 /     1000

 the factual no of atoms/molecules in this cell:        3 /        1

          lattice vectors
    20.00000     0.00000     0.00000
     0.00000    20.00000     0.00000
     0.00000     0.00000    20.00000

          Using: is_simple_orthogonal:    T


          Using: is_orthogonal:    T


          reciprocal lattice vectors
     0.05000     0.00000     0.00000
     0.00000     0.05000     0.00000
     0.00000     0.00000     0.05000


 --------------------------------------------------
 configuration sample (up to 10 atoms):
 --------------------------------------------------

 molecule SPCE    
 OW        c        1   type   1
      0.0000000      0.0000000      0.0000000     0
 HW        c        2   type   2
     -1.0000000      0.0000000      0.0000000     0
 HW        c        3   type   2
      0.3333133      0.9432227      0.0000000     0


 --------------------------------------------------
 NOTE: FED calculation is OFF; order parameter =  0 (should be zero)


 ==================================================
                simulation parameters 
 ==================================================

 cell (box) type [ < 0 -> 2D-slit; else 3D-bulk ] =         -2 (slit: XY-PBC)

 SLIT (planar pore): number of hard / soft walls  =          0         2

 SLIT (planar pore): number of charged walls, SCD =          0    0.000000    0.000000

 SLIT (planar pore): MFA correction cutoff type   =          0         0      0.0000000  1.  0.

 global cutoff for interactions (Angstroms)       =   0.1000000E+02

 cutoff for vdw interactions (Angstroms)          =   0.1000000E+02

 Verlet neighbour lists not used                  =   F  F

 system temperature (Kelvin)                      =   0.3000000E+03

 constant volume simulation (NVT), i.e. volume not sampled

 system pressure (katms)                          =   0.1000000E-01

 total number of MC steps (cycles)                =    1100000

 number of equilibration steps                    =     100000

 number of pre-heating steps                      =         -1

 maximum no of non-bonded neighbours              =          0

 maximum no of three body neighbours              =          6

 three body list updated every (cycles)           =   20000000

 stack-size for block averaging                   =        100

 statistics data saved every (cycles) -> PTFILE*  =       1000

 energy frames printed every (cycles) -> OUTPUT*  =     100000

 energy checks printed every (cycles) -> OUTPUT*  =     500000

 configurations stored every (cycles) -> REVCON*  =      10000

 trajectories archived every (cycles) -> HISTORY* =      10000         0 (DL_POLY-2 format)

 TRAJECTORY* is stored every (cycles) as DCD file =      10000       110

 REVCON configuration data in DL_POLY-2 HISTORY format

 HISTORY* & TRAJECTORY* in DL_POLY-2 (all/subset) & DCD (all) formats

 Coulomb treatment for configuration  1           =         -4

 dielectric constant (medium permittivity)        =   0.1000000E+01

 ewald precision                                  =   0.1000000E-05

 all g-vectors stored on each node

 molecule types to be moved 
     SPCE     XYZ =   1  1  1

 initial maximum for mol displacement             =   0.1000000E+00

 displacement distance update  (cycles)           =        100

 acceptance ratio for mol  displacement           =   0.3700000E+00

 molecules moved with frequency                   =         25

 molecule types to be rotated 
     SPCE    

 initial maximum for mol rotation (degrees)       =   0.3000000E+02

 initial maximum for mol rotation (radians)       =   0.5235988E+00

 rotation angle update  (cycles)                  =        100

 acceptance ratio for mol rotation                =   0.3700000E+00

 molecules rotated with frequency                 =         25

 tolerance for rejection (energy units)           =   0.1000000E+03

 z-density will be calculated

 the no of z-density bins                         =        500

 the frequency of z-density calculations          =       1000

 rdfs will be calculated

 the no of rdf bins                               =        400

 the maximum distance for rdf calculation         =   0.1000000E+02

 the frequency of rdf calculations                =       1000

 random number generator seeded from user input

 random number seeds (i,j,k,l) =     12    34    56    78 (for workgroup    0)

 total job time and close time =      1000000.00         200.00

 rotation of molecules will use quaternions

 Grand-Canonical (GCMC, muVT) scheme using partial pressure (katm)

 species 'SPCE' is movable by translation

 species 'SPCE' is movable by rotation

 grand canonical ensemble using molecules

 the number of different molecule types           =     1

 the frequency of mol insert/delete               =    50

 the minimum distance for inserts                 =     2.5000

 the number of atoms in molecule   1 is     3

 the maximum number of atoms in molecule          =     3

 molecule name SPCE    

 the partial gas pressure of molecules (katms)    =      0.1000000

 number of moving particles for box    1         1

 energy unit conversion factor (user -> internal) =      100.0000000000

 beta (deca-J/mol) & Bjerrum length (Angstroem)   =        0.0040090551      557.0000087538

 ==================================================



 Initialisation phase 1 - elapsed time:  0 h :  0 m :  0.000 s




 cutoffs for lattice summation in box:  1

 eta                                 :    0.32000
 real space cutoff A                 :   10.00000
 reciprocal space cutoff 1/A         :    0.00000
 kmax1, kmax2, kmax3                 :   0  1  0


 total & max number of reciprocal lattice vectors:         0         0


 Initialisation phase 2 - elapsed time:  0 h :  0 m :  0.000 s


 --------------------------------------------------
                  initial energies 
 --------------------------------------------------

 break down of energies for box:   1

 total energy                       -0.8580711220E-02
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb                  0.0000000000E+00
 external mfa coulomb                0.0000000000E+00
 nonbonded two body (vdw)            0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 bonded four body (angle)            0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy          -0.8580711220E-02
 total virial                        0.0000000000E+00
 volume                              0.8000000000E+04

 components of energies by  molecule: SPCE    

 total energy                       -0.8580711220E-02
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb - other types    0.0000000000E+00
 real space coulomb - same type      0.0000000000E+00
 real space coulomb - self energy    0.0000000000E+00
 real space coulomb - corection      0.0000000000E+00
 vdw - other types mol               0.0000000000E+00
 vdw - same type mol                 0.0000000000E+00
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy          -0.8580711220E-02


 Initialisation phase 3 - elapsed time:  0 h :  0 m :  0.000 s


 Resetting time and proceeding to simulation

 Opended DCD file : 'TRAJECTORY.000' for writing
 DCD file header  : # frames / 1st / step =          110  /        10000  /        10000
 DCD file header  : # atoms / cell =         3000  /            1
 Proceeding to add frame(s) to the DCD trajectory...

 Reopended DCD file 'TRAJECTORY.000' for writing (icell =            1 )
 Now adding frame(s) to the DCD trajectory...
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 306 =?=        261 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 306 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 342 =?=        306 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 342 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 366 =?=        342 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 366 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 369 =?=        366 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 369 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 387 =?=        369 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 387 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 390 =?=        387 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 390 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 399 =?=        390 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 399 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 402 =?=        399 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 402 =?=       3000 - must be GCMC; padding with (0,0,0) !!!


 Iteration       100000 - elapsed time:  0 h :  0 m :  4.194 s


 ====================================================================================================
      step      en-total            h-total             coul-rcp            coul-real
      step      en-vdw              en-three            en-pair             en-angle 
      step      en-four             en-many             en-external         en-extMFA
      step      volume              cell-a              cell-b              cell-c   
      step      alpha               beta                gamma 
      r-av      en-total            h-total             coul-rcp            coul-real
      r-av      en-vdw              en-three            en-pair             en-angle 
      r-av      en-four             en-many             en-external         en-extMFA
      r-av      volume              cell-a              cell-b              cell-c   
      r-av      alpha               beta                gamma 
 ----------------------------------------------------------------------------------------------------
    100000    -0.4566256250E+04   -0.3907239234E+04    0.0000000000E+00   -0.5429536284E+04
               0.8911574024E+03    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.2787736849E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.4564432339E+04   -0.3905415323E+04    0.0000000000E+00   -0.5432037736E+04
               0.8954818873E+03    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.2787649005E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        136.0000        136.0000
 HW       c        272.0000        272.0000

 SPCE            136.0000        136.0000
 ----------------------------------------------------------------------------------------------------
  equilibration period ended at step       100000
 ----------------------------------------------------------------------------------------------------
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' : 408 =?=        402 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 408 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms variation detected in DCD file 'TRAJECTORY.000' - keeping silent about more such events !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' : 414 =?=       3000 - must be GCMC; padding with (0,0,0) !!!
 WARNING: # atoms < maximum # atoms for DCD file 'TRAJECTORY.000' - keeping silent about more such events !!!


 Iteration       200000 - elapsed time:  0 h :  0 m : 10.025 s


    200000    -0.5514613420E+04   -0.4767728613E+04    0.0000000000E+00   -0.6551115238E+04
               0.1067107515E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3060569711E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.5514120967E+04   -0.4767236160E+04    0.0000000000E+00   -0.6545729009E+04
               0.1062201726E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3059368389E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        154.0000        154.0000
 HW       c        308.0000        308.0000

 SPCE            154.0000        154.0000
 ----------------------------------------------------------------------------------------------------


 Iteration       300000 - elapsed time:  0 h :  0 m : 16.388 s


    300000    -0.6005695142E+04   -0.5214876439E+04    0.0000000000E+00   -0.7225047575E+04
               0.1252762575E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3341014237E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.6014911100E+04   -0.5224092397E+04    0.0000000000E+00   -0.7231044570E+04
               0.1249578494E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3344502438E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        163.0000        163.0000
 HW       c        326.0000        326.0000

 SPCE            163.0000        163.0000
 ----------------------------------------------------------------------------------------------------


 Iteration       400000 - elapsed time:  0 h :  0 m : 23.033 s


    400000    -0.6464008828E+04   -0.5629256229E+04    0.0000000000E+00   -0.7878664951E+04
               0.1452529480E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3787335704E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.6464337183E+04   -0.5629584584E+04    0.0000000000E+00   -0.7882554751E+04
               0.1456085829E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3786826120E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        172.0000        172.0000
 HW       c        344.0000        344.0000

 SPCE            172.0000        172.0000
 ----------------------------------------------------------------------------------------------------


 Iteration       500000 - elapsed time:  0 h :  0 m : 29.897 s


    500000    -0.6738422229E+04   -0.5889024998E+04    0.0000000000E+00   -0.8080753723E+04
               0.1384130974E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4179947981E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.6737658443E+04   -0.5888261212E+04    0.0000000000E+00   -0.8069934502E+04
               0.1374039889E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4176382995E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        175.0000        175.0000
 HW       c        350.0000        350.0000

 SPCE            175.0000        175.0000
 ----------------------------------------------------------------------------------------------------

 Workgroup    0, box    1 check: U_recalc - U_accum =  0.50059E-08  0.20069E-10  0.38226E-13 -0.74288E-14 (internal, kT, kT/atom, dU/U)

 ----------------------------------------------------------------------------------------------------



 Iteration       600000 - elapsed time:  0 h :  0 m : 36.876 s


    600000    -0.7025099965E+04   -0.6146413470E+04    0.0000000000E+00   -0.8641482854E+04
               0.1657655291E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4127240136E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.7020496391E+04   -0.6141809897E+04    0.0000000000E+00   -0.8640125608E+04
               0.1660910284E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4128106643E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        181.0000        181.0000
 HW       c        362.0000        362.0000

 SPCE            181.0000        181.0000
 ----------------------------------------------------------------------------------------------------


 Iteration       700000 - elapsed time:  0 h :  0 m : 44.028 s


    700000    -0.7145004728E+04   -0.6261436690E+04    0.0000000000E+00   -0.8714261985E+04
               0.1604588832E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3533157558E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.7137122005E+04   -0.6253553966E+04    0.0000000000E+00   -0.8706261968E+04
               0.1604303649E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3516368651E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        182.0000        182.0000
 HW       c        364.0000        364.0000

 SPCE            182.0000        182.0000
 ----------------------------------------------------------------------------------------------------


 Iteration       800000 - elapsed time:  0 h :  0 m : 51.272 s


    800000    -0.7356152732E+04   -0.6457940062E+04    0.0000000000E+00   -0.8887084713E+04
               0.1567741568E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3680958795E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.7345494155E+04   -0.6447281485E+04    0.0000000000E+00   -0.8865346305E+04
               0.1556357321E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3650517154E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        185.0000        185.0000
 HW       c        370.0000        370.0000

 SPCE            185.0000        185.0000
 ----------------------------------------------------------------------------------------------------


 Iteration       900000 - elapsed time:  0 h :  0 m : 58.574 s


    900000    -0.7420733679E+04   -0.6517639464E+04    0.0000000000E+00   -0.8774125786E+04
               0.1396965175E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4357306729E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.7417487047E+04   -0.6514392833E+04    0.0000000000E+00   -0.8787082979E+04
               0.1413168597E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4357266502E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        186.0000        186.0000
 HW       c        372.0000        372.0000

 SPCE            186.0000        186.0000
 ----------------------------------------------------------------------------------------------------


 Iteration      1000000 - elapsed time:  0 h :  1 m :  5.962 s ( 65.962)


   1000000    -0.7428320566E+04   -0.6520344808E+04    0.0000000000E+00   -0.8911155217E+04
               0.1522273905E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3943925405E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.7431349101E+04   -0.6523373342E+04    0.0000000000E+00   -0.8921317729E+04
               0.1529375363E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3940673473E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        187.0000        187.0000
 HW       c        374.0000        374.0000

 SPCE            187.0000        187.0000
 ----------------------------------------------------------------------------------------------------

 Workgroup    0, box    1 check: U_recalc - U_accum =  0.48894E-08  0.19602E-10  0.34941E-13 -0.65822E-14 (internal, kT, kT/atom, dU/U)

 ----------------------------------------------------------------------------------------------------



 Iteration      1100000 - elapsed time:  0 h :  1 m : 13.292 s ( 73.292)


   1100000    -0.7380357672E+04   -0.6472381914E+04    0.0000000000E+00   -0.8910855257E+04
               0.1572459507E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4196192210E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02


              -0.7380003258E+04   -0.6472027500E+04    0.0000000000E+00   -0.8910053165E+04
               0.1572011366E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.4196145965E+02    0.0000000000E+00
               0.8000000000E+04    0.2000000000E+02    0.2000000000E+02    0.2000000000E+02
               0.9000000000E+02    0.9000000000E+02    0.9000000000E+02
           
 OW       c        187.0000        187.0000
 HW       c        374.0000        374.0000

 SPCE            187.0000        187.0000
 ----------------------------------------------------------------------------------------------------


 ====================================================================================================
                          averages and fluctuations
 ----------------------------------------------------------------------------------------------------
      avrg      en-total            h-total             coul-rcp            coul-real
      avrg      en-vdw              en-three            en-pair             en-angle 
      avrg      en-four             en-many             en-external         en-extMFA
      flct      en-total            h-total             coul-rcp            coul-real
      flct      en-vdw              en-three            en-pair             en-angle 
      flct      en-four             en-many             en-external         en-extMFA
 ----------------------------------------------------------------------------------------------------
   1000000    -0.6710795550E+04   -0.5861732148E+04    0.0000000000E+00   -0.8114749289E+04
               0.1439738117E+04    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00   -0.3578437770E+02    0.0000000000E+00

               0.7368547908E+03    0.6745823581E+03    0.0000000000E+00    0.8924925738E+03
               0.1748934056E+03    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00
               0.0000000000E+00    0.0000000000E+00    0.4632412497E+01    0.0000000000E+00



 average cell parameters and fluctuations

          vol   =     0.8000000000E+04    0.0000000000E+00
          a     =        20.0000000000        0.0000000000
          b     =        20.0000000000        0.0000000000
          c     =        20.0000000000        0.0000000000
          alpha =        90.0000000000        0.0000000000
          beta  =        90.0000000000        0.0000000000
          gamma =        90.0000000000        0.0000000000
    
 OW       c        174.9316         12.8410
 HW       c        349.8632         25.6820

 SPCE            174.9316         12.8410


 ----------------------------------------------------------------------------
                          final energies 
 ----------------------------------------------------------------------------

 break down of energies for box:   1

 total energy                       -0.7380357672E+04
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb                 -0.8910855257E+04
 external mfa coulomb                0.0000000000E+00
 nonbonded two body (vdw)            0.1572459507E+04
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 bonded four body (angle)            0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy          -0.4196192210E+02
 total virial                        0.0000000000E+00
 volume                              0.8000000000E+04

 components of energies by  molecule: SPCE    

 total energy                       -0.7380357672E+04
 reciprocal space coulomb            0.0000000000E+00
 real space coulomb - other types    0.0000000000E+00
 real space coulomb - same type     -0.8910855257E+04
 real space coulomb - self energy    0.0000000000E+00
 real space coulomb - corection      0.0000000000E+00
 vdw - other types mol               0.0000000000E+00
 vdw - same type mol                 0.1572459507E+04
 bonded two body (pair)              0.0000000000E+00
 nonbonded three body                0.0000000000E+00
 bonded three body (angle)           0.0000000000E+00
 many body energy                    0.0000000000E+00
 external potential energy          -0.4196192210E+02


 ----------------------------------------------------------------------------
                          processing data 
 ----------------------------------------------------------------------------

 total no of attempted & empty mol moves  :    275430         0
 successful no of mol moves & acceptance  :    103443      0.37556911

 displacement (Angstroms) for SPCE     :   0.2106

 total no of mol rotations      :    275097
 successful no of mol rotations :    103299      0.37550028

 angle of rot for (degrees) SPCE     :  34.6486

 attempted molecule inserts  =     274290
 successful molecule inserts =        619      0.00225674

 attempted molecule deletes  =     275183
 successful molecule deletes =        433      0.00157350


 pure simulation time (excl. init.):  0 h :  1 m : 13.316 s ( 73.316)




 total elapsed time   (incl. init.):  0 h :  1 m : 13.316 s ( 73.316)


 normal exit 
