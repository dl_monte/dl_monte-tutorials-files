echo "Calculations using 4 blocks with no equilibration time..."
echo

echo "Calculate and output p_1/p_2:"
../../../probratio.sh 1 108000000
../../../probratio.sh 108000001 216000000
../../../probratio.sh 216000001 324000000
../../../probratio.sh 324000001 432000000

echo
echo "Calculate and output \Delta G/N (units of epsilon):"
../../../probratio.sh 1 108000000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 108000001 216000000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 216000001 324000000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 324000001 432000000 | awk '{print -0.1*log($1)/216}'

echo
echo "Calculate and output mean and std. error in \Delta G/N (units of epsilon):"
# Store \DeltaG/N for all in file probratio_example.tmp, and use that file to calculate mean and std. error
../../../probratio.sh 1 108000000 | awk '{print -0.1*log($1)/216}' > probratio_example.tmp
../../../probratio.sh 108000001 216000000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 216000001 324000000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 324000000 432000000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
awk '{sum=sum+$1; sum2=sum2+$1*$1} END{mean=sum/NR; var=sum2/NR-mean*mean; stdev=sqrt(var); stderr=stdev/sqrt(NR); print mean,stderr}' probratio_example.tmp
rm -f probratio_example.tmp


echo
echo
echo

echo "Calculations using 4 blocks with equilibration time 4320000..."
echo
echo
echo "Calculate and output \Delta G/N (units of epsilon):"
../../../probratio.sh 4320000 111240000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 111240001 218160000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 218160001 325080000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 325080001 432000000 | awk '{print -0.1*log($1)/216}'

echo
echo "Calculate and output mean and std. error in \Delta G/N (units of epsilon):"
# Store \DeltaG/N for all in file probratio_example.tmp, and use that file to calculate mean and std. error
../../../probratio.sh 4320000 111240000 | awk '{print -0.1*log($1)/216}' > probratio_example.tmp
../../../probratio.sh 111240001 218160000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 218160001 325080000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 325080001 432000000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
awk '{sum=sum+$1; sum2=sum2+$1*$1} END{mean=sum/NR; var=sum2/NR-mean*mean; stdev=sqrt(var); stderr=stdev/sqrt(NR); print mean,stderr}' probratio_example.tmp
rm -f probratio_example.tmp

echo
echo
echo

echo "Calculations using 5 blocks with equilibration time 4320000..."
echo
echo
echo "Calculate and output \Delta G/N (units of epsilon):"
../../../probratio.sh 4320000 89856000 | awk '{print -0.1*log($1)/216}' 
../../../probratio.sh 89856001 175392000 | awk '{print -0.1*log($1)/216}' 
../../../probratio.sh 175392001 260928000 | awk '{print -0.1*log($1)/216}' 
../../../probratio.sh 260928001 346464000 | awk '{print -0.1*log($1)/216}' 
../../../probratio.sh 346464001 432000000 | awk '{print -0.1*log($1)/216}' 

echo
echo "Calculate and output mean and std. error in \Delta G/N (units of epsilon):"
# Store \DeltaG/N for all in file probratio_example.tmp, and use that file to calculate mean and std. error
../../../probratio.sh 4320000 89856000 | awk '{print -0.1*log($1)/216}' > probratio_example.tmp
../../../probratio.sh 89856001 175392000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 175392001 260928000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 260928001 346464000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 346464001 432000000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
awk '{sum=sum+$1; sum2=sum2+$1*$1} END{mean=sum/NR; var=sum2/NR-mean*mean; stdev=sqrt(var); stderr=stdev/sqrt(NR); print mean,stderr}' probratio_example.tmp
rm -f probratio_example.tmp

echo
echo
echo

echo "Calculations using 8 blocks with equilibration time 4320000..."
echo
echo
echo "Calculate and output \Delta G/N (units of epsilon):"
../../../probratio.sh 4320000 57780000 | awk '{print -0.1*log($1)/216}' 
../../../probratio.sh 57780001 111240000 | awk '{print -0.1*log($1)/216}' 
../../../probratio.sh 111240001 164700000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 164700001 218160000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 218160001 271620000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 271620001 325080000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 325080001 378540000 | awk '{print -0.1*log($1)/216}'
../../../probratio.sh 378540001 432000000 | awk '{print -0.1*log($1)/216}'

echo
echo "Calculate and output mean and std. error in \Delta G/N (units of epsilon):"
# Store \DeltaG/N for all in file probratio_example.tmp, and use that file to calculate mean and std. error
../../../probratio.sh 4320000 57780000 | awk '{print -0.1*log($1)/216}' > probratio_example.tmp
../../../probratio.sh 57780001 111240000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 111240001 164700000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 164700001 218160000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 218160001 271620000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 271620001 325080000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 325080001 378540000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
../../../probratio.sh 378540001 432000000 | awk '{print -0.1*log($1)/216}' >> probratio_example.tmp
awk '{sum=sum+$1; sum2=sum2+$1*$1} END{mean=sum/NR; var=sum2/NR-mean*mean; stdev=sqrt(var); stderr=stdev/sqrt(NR); print mean,stderr}' probratio_example.tmp
rm -f probratio_example.tmp

echo
echo
echo

