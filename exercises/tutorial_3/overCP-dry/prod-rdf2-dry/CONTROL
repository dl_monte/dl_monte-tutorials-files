NPT simulation of Lennard-Jones fluid at critical point - aim to reproduce Fig. 2 in Wilding & Binder, Physica A 231 439 (1996) 
use ortho
finish

seeds 12 34 56 78               # Seed RNG seeds explicitly to the default                          

#nbrlist auto                    # Use a neighbour list to speed up calculations (not always optimum)
#maxnonbondnbrs 512              # Maximum number of neighbours in neighbour list  

temperature     1.5             # Reduced temperature T* = 1.5 > T*(CP) = 1.1876 (over-critical)
pressure        0.02            # p* = p(katm)/0.163882576 > p*(CP)=0.1093 (reduced pressure at CP)

steps           5000000         # Number of moves to perform in simulation 
equilibration         0         # Equilibration period: statistics are gathered after this period
sample coordinate 10000         # Frequency to output configuration to trajectory file(s)
sample volume  10  10.          # Sample volume: every 10th volume move (on average); bin size
sample rdf 400 8.0 1000         # Sample RDF(s): number of bins; cutoff; stride (number of steps)

print            500000         # Print statistics every 'print' moves
check           1000000         # Check rolling eneries vs re-calculated from scratch
stack             10000         # Size of blocks for block averaging to obtain statistics
stats             10000         # frequency of printing stats to PTFILE

archiveformat   dlpoly2+dcd     # format of the trajectory file (ARCHIVE/HISTORY/TRAJECTORY)
revconformat    dlmonte         # format of REVCON file (to replace CONFIG if restarting)

maxatmdist           5.0        # initial max atom displacement (set based on equilibration run)
acceptatmmoveupdate  1000       # stride (number of MC steps) for adjusting the max atom displacement
acceptatmmoveratio   0.4        # target acceptance ratio for atom moves [default: 0.37]

move atom 1 512                 # Move atoms
LJ core                         # name & type of atom

move volume cubic log 1         # Move volume (isotropic) using ln(V) moves

start simulation

