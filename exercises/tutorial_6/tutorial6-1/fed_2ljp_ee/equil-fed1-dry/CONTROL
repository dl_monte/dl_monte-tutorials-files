Lennard-Jones - 2 particles

# 'use' block - simulation control

# use sequential particle moves (not random particle selection)
#use seqmove
#use ortho
#use repexch 4  -0.2  500

# FED block is started by 'use fed <flavor> <stride>' (must be in 'use' section on top)
use fed generic 1

# FED <method> <damping> <scaling> <update> [<smooth_itr> <smooth_1st> <smooth_last> <weight>]
fed method EE   0.5       1.125     500000    #3  10  250  #0.333

# FED order [param] <type> <nbins> <first> <last> <power>
fed order parameter com2    250     2.5     27.5   1
com1 mol 1 atom 1   # use COM1 for a group of molecules or atoms within molecules
com2 mol 1 atom 2   # use COM2 for a group of molecules or atoms within molecules
com  sampling correction 1 # entropy on a spherical surface (on the fly)
#com  sampling correction 2 # entropy in a spherical bin volume (a posteriori)
fed order parameter done

# close FED block started by 'use fed' directive
fed done

# close the 'use' block
finish

temperature          1.0         # Reduced temperature
nocoul all                       # No electrostatics
#distewald                       # Distributed Ewald sums in parallel runs

equilibration        0           # Equilibration period (not for FED calcs)
steps                4000000     # Total number of MC steps 2^(<iters>-1)*<update>
print                1000000     # Frequency of printing to OUTPUT
stats                 100000     # frequency of printing stats to PFILE
check                1000000     # Check rolling energies vs recalculated from scratch
sample rdfs     300 30.0 100     # Sampling grid, bin size & frequency for RDF calculation
sample coords          10000     # Frequency of storing configuration in trajectory

acceptatmmoveupdate  1000000000  # DO NOT update max atom displacement
maxatmdist           10.000      # Max atom displacement
move atoms         1 100         # Move atoms
Lj core                          # Name & type of atom to move

start simulation
